module.exports = {
	extends: "airbnb-base",
  rules: {
	  // Allow console messages, we may use for logging
	  "no-console": 0,
  }
};
