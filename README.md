# Inspirational Quotes API

Store inspirational quotes and generate a random quote from the collection!

This is a demo project written for a tutorial: [Build an inspirational quotes API in Node.js with Google Cloud Functions](https://www.voxis.io/)

## Run in local development

First install dependencies:

    npm install

Serve the function locally:

    npm start

Or alternatively, to serve locally and watch for code changes:

    npm run watch

## Deploy to Cloud Functions

Before starting, you need to create and configure a project in Google Cloud Platform (see [blog post](https://www.voxis.io/) for full details). You also need to [install the Google Cloud SDK](https://cloud.google.com/sdk/install).

Before deploying, ensure you have selected the correct project. You can list all your projects with:

    gcloud projects list

You should see the project you created earlier. You can then set the project with:

    gcloud config set project [PROJECT_ID]

Where `[PROJECT_ID]` is the ID of your project.

You can now deploy using the deployment script:

    ./deploy.sh

Deployment may take a few minutes. You will be given a URL for each of the Cloud Functions deployed.
