#!/bin/bash

# Deploys each of the functions.
gcloud functions deploy quotes --runtime nodejs10 --trigger-http
gcloud functions deploy random --runtime nodejs10 --trigger-http

