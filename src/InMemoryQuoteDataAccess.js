class InMemoryQuoteDataAccess {
  constructor() {
    this.quotes = [];
    this.nextId = 1;
  }

  async addQuote(text, author) {
    const newQuote = {
      id: this.nextId,
      text,
      author,
    };
    this.quotes.push(newQuote);
    this.nextId += 1;
    return newQuote;
  }

  async getQuotes() {
    return this.quotes;
  }

  async getQuote(quoteId) {
    return this.quotes.find((q) => q.id === quoteId);
  }

  async removeQuote(quoteId) {
    const index = this.quotes.findIndex((q) => q.id === quoteId);
    return this.quotes.splice(index, 1)[0];
  }

  async updateQuote(quoteId, text, author) {
    const quote = this.quotes.find((q) => q.id === quoteId);
    Object.assign(quote, { text, author });
    return quote;
  }

  async selectRandomQuote() {
    const index = Math.floor(Math.random() * this.quotes.length);
    return this.quotes[index];
  }
}

module.exports = InMemoryQuoteDataAccess;
