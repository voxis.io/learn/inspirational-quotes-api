const InMemoryQuoteDataAccess = require('./InMemoryQuoteDataAccess');

const quoteDataAccess = new InMemoryQuoteDataAccess();

const getQuotesCollection = async (req, res) => {
  res.json({
    quotes: await quoteDataAccess.getQuotes(),
  });
};

const postNewQuote = async (req, res) => {
  const { text, author } = req.body;
  if (typeof text !== 'string') {
    res.status(400).json({ error: 'Invalid request' });
  } else {
    const newQuote = await quoteDataAccess.addQuote(text, author);
    res.json(newQuote);
  }
};

const getQuote = async (quote, req, res) => {
  res.json(quote);
};

const putQuote = async (quote, req, res) => {
  const { text, author } = req.body;
  const updatedQuote = await quoteDataAccess.updateQuote(quote.id, text, author);
  res.json(updatedQuote);
};

const deleteQuote = async (quote, req, res) => {
  const deletedQuote = await quoteDataAccess.removeQuote(quote.id);
  res.json(deletedQuote);
};

/**
 * Quotes resource
 *
 * @param {Object} req Cloud Function request context. @see https://expressjs.com/en/api.html#req
 * @param {Object} res Cloud Function response context. @see https://expressjs.com/en/api.html#res
 * @returns {void}
 */
const quotes = async (req, res) => {
  if (req.path === '/quotes' || req.path === '/') {
    if (req.method === 'GET') {
      await getQuotesCollection(req, res);
    } else if (req.method === 'POST') {
      await postNewQuote(req, res);
    } else {
      res.status(405).json({ error: 'Method Not Allowed' });
    }
  } else {
    const matches = req.path.match(/(\/quotes)?\/([0-9]+)/);
    if (matches) {
      const quoteId = parseInt(matches[2], 10);
      const quote = await quoteDataAccess.getQuote(quoteId);
      if (!quote) {
        res.status(404).json({ error: 'Quote Not Found' });
      } else if (req.method === 'GET') {
        await getQuote(quote, req, res);
      } else if (req.method === 'PUT') {
        await putQuote(quote, req, res);
      } else if (req.method === 'DELETE') {
        await deleteQuote(quote, req, res);
      } else {
        res.status(405).json({ error: 'Method Not Allowed' });
      }
    } else {
      res.status(404).json({ error: 'Not Found', path: req.path });
    }
  }
};

/**
 * Random quote resource
 *
 * @param {Object} req Cloud Function request context. @see https://expressjs.com/en/api.html#req
 * @param {Object} res Cloud Function response context. @see https://expressjs.com/en/api.html#res
 * @returns {void}
 */
const random = async (req, res) => {
  // Ensure result is never cached so that a new random quote is always generated:
  res.setHeader('Cache-Control', 'no-cache, no-store');

  const randomQuote = await quoteDataAccess.selectRandomQuote();
  if (randomQuote) {
    res.json(randomQuote);
  } else {
    res.status(404).json({ error: 'No quotes available' });
  }
};

/*
 * Routes the request to the appropriate function.
 * Functions Framework only allows us to test one function, but we want to be able to test all
 * functions in local development at one time.
 *
 * Only used for local development purposes.
 */
const index = async (req, res) => {
  if (req.path.startsWith('/quotes')) {
    await quotes(req, res);
  } else if (req.path.startsWith('/random')) {
    await random(req, res);
  } else {
    res.send('No function is defined at this path');
  }
};

module.exports = {
  quotes,
  random,
  index,
};
